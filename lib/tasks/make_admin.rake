begin
  require 'rake'
  require File.expand_path('../../../config/application', __FILE__)

  desc "Give admin rights to a user (specify email-address)"
  task :make_admin,[ :email] => :environment do |t,args|
    puts args[:email]
    user = User.find_by_email(args[:email])
    if user.nil?
      puts "Can't find user with e-mail " + args[:email]
    else
      user.add_role(:admin)
      user.save!
    end
  end
end
