begin
  require 'rake'
  require File.expand_path('../../../config/application', __FILE__)

  desc "Create user with password (specify email-address and password)"
  task :create_user,[ :email, :password] => :environment do |t,args|
    if !args[:email].blank? && !args[:password].blank?
      user = User.find_by_email(args[:email])
      if user.nil?
        user = User.new(email: args[:email], password: args[:password])
        user.save!
        puts "The user with e-mail " + args[:email] + "is created"
      else
        puts "The user with e-mail " + args[:email] + " already exists!"
      end
    else
      puts "E-mail and\/or password is blank. Please check it."
    end
  end
end
