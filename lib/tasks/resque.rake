require 'resque/tasks'
require 'resque/scheduler/tasks'

namespace :resque do

  task :setup => :environment

  desc "Retries the failed jobs and clears the current failed jobs queue at the same time"
  task "retry-failed-jobs" => :environment do
    (Resque::Failure.count-1).downto(0).each { |i| Resque::Failure.requeue(i) }; Resque::Failure.clear
  end


  task :setup_schedule => :setup do
    require 'resque-scheduler'

    Resque.schedule = YAML.load_file("#{Rails.root}/config/resque_schedule.yml")

    require 'transactions_processing_job'
  end

  Resque.after_fork = Proc.new { ActiveRecord::Base.establish_connection }

  task :scheduler_setup => :setup_schedule

end
