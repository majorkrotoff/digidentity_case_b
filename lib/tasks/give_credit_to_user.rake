begin
  require 'rake'
  require File.expand_path('../../../config/application', __FILE__)

  desc "Give credit to to user account (specify email-address and amount)"
  task :give_credit_to_user,[ :email, :amount] => :environment do |t,args|
    if !args[:email].blank?
      user = User.find_by_email(args[:email])
      if user.nil?
        puts 'The user with e-mail ' + args[:email] + ' doesn\'t exist!'
      else
        # Get account
        if user.account == nil
          puts 'Creating an account for the user'
          user.account = Account.create(user_id: user.id, balance: 0, code: SecureRandom.uuid.to_s)
        end

        # Check new balance
        if !args[:amount].to_f.nan?
          user.account.balance += args[:amount].to_f

          transaction = Transaction.create( user_id: user.id,
                                            account_from_id: user.account.id,
                                            account_to_id: user.account.id,
                                            amount: args[:amount].to_f,
                                            status_id: TransactionStatus.where(name: 'Finished').first.id )

          if user.account.save! && transaction.save!
            puts 'Credit for the user with e-mail ' + args[:email] + ' is added'
          else
            puts 'Can\'t save the transaction'
          end
        else
          puts 'Wrong credit amount'
        end
      end
    end
  end
end
