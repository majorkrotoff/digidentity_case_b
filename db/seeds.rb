# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

TransactionStatus.create(name: 'Ordered')
TransactionStatus.create(name: 'Finished')
TransactionStatus.create(name: 'Cancelled')
