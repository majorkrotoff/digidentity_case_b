class AddBlockedAmountToAccount < ActiveRecord::Migration
  def change
    add_column :accounts, :blocked_amount, :float
  end
end
