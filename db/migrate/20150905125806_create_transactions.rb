class CreateTransactions < ActiveRecord::Migration
  def change
    create_table :transactions do |t|
      t.integer :account_from_id
      t.integer :account_to_id
      t.float :amount
      t.integer :status_id

      t.timestamps null: false
    end
  end
end
