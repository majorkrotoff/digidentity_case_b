# Case B: Ruby Bank

The Ruby Bank uses background tasks to process all the money transactions. As user tries to send money to another user the application creates a new transaction with the status "Ordered", blocks certain amount of money on frist user's account and puts the transaction into the queue. The transaction is processed in background mode.
If amount of money on user's account is enouth to finish the transaction then the money will be send to the other user's account and the transaction will be marked as "Finished". Otherwise the transaction will be cancelled and will be marked accordingly.


## Rake tasks:

1. Create a user "a@a.com" with password "12345678":

```
#!shell
rake create_user["a@a.com","12345678"]
```
      
2. Assign admin rights to a user "a@a.com" (to see all accounts and all transactions):

```
#!shell
rake create_user["a@a.com"]
```
      
3. Give credit to a user "a@a.com":

```
#!shell
rake create_user["a@a.com","150.00"]
```


## Working with the application

To start the application, please, do the following:

1. Start the main application:

```
#!shell
rails s
```

2. Start the background task scheduler (to process all the hanged on transactions)

```
#!shell
rake resque:scheduler
```

3. Start the worker to process all the background tasks

```
#!shell
bundle exec rake environment resque:work QUEUE=ruby_bank_transaction_process
```
