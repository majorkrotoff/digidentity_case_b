class TransactionProcessingJob

  @queue = :ruby_bank_transaction_process

  def self.perform(transaction_id)
    transaction = Transaction.find(transaction_id)

    if transaction
      account_from = Account.find(transaction.account_from_id)
      account_to = Account.find(transaction.account_to_id)

      if !account_from.blank? && !account_to.blank?
        if transaction.amount <= account_from.blocked_amount
          account_from.balance -= transaction.amount
          account_from.blocked_amount -= transaction.amount
          account_to.balance += transaction.amount

          transaction.status_id = TransactionStatus.where(name: 'Finished').first.id;

          account_from.save
          account_to.save
          transaction.save
        else
          transaction.status_id = TransactionStatus.where(name: 'Cancelled').first.id;
          transaction.save
        end
      end
    end
  end

end
