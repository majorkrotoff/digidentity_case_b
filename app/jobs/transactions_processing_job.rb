module RubyBank

  class TransactionsProcessingJob
    @queue = :ruby_bank_transaction_process

    def self.perform
      Transaction.where(status_id: TransactionStatus.where(name: 'Ordered').first.id).each do |transaction|
        Resque.enqueue(TransactionProcessingJob, transaction.id)
      end
    end
  end

end
