class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new
    if user.has_role?(:admin)
      # Admins can manage all
      can :manage, :all
    elsif user.has_role?(:user)
      # Users can read all and manage what they own
      can :read, :all
    else
      # Guests can read all
      can :read, :all
    end 
  end

end
