class Transaction < ActiveRecord::Base

  belongs_to :user
  belongs_to :transaction_status

  has_one :account_from, as: :account_from_id, class_name: 'Account'
  has_one :account_to,  as: :account_to_id, class_name: 'Account'

end
