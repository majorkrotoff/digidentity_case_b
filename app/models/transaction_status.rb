class TransactionStatus < ActiveRecord::Base

  has_many :transactions

end
