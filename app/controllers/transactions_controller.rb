class TransactionsController < ApplicationController
  before_action :set_transaction, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  @queue = :ruby_bank_transaction_process

  # GET /transactions
  def index
    if params[:account_id].blank?
      if current_user.has_role?(:admin)
        @transactions = Transaction.all
      else
        @transactions = nil
      end
    else
      @transactions = Transaction.where("account_from_id = ? OR account_to_id = ?", params[:account_id], params[:account_id])
    end
  end

  # GET /transactions/1
  def show
  end

  # GET /transactions/new
  def new
    @transaction = Transaction.new
    @transaction.user_id = current_user.id
    @transaction.account_from_id = current_user.account.id
  end

  # GET /transactions/1/edit
  def edit
  end

  # POST /transactions
  def create
    # check if amount is correct
    p transaction_params[:amount]
    if transaction_params[:amount].to_f.round(2) <= (current_user.account.balance - (current_user.account.blocked_amount.blank? ? 0 : current_user.account.blocked_amount ) )
      @transaction = Transaction.new(transaction_params)
      @transaction.user_id = current_user.id
      @transaction.account_from_id = current_user.account.id

      # block amount
      current_user.account.blocked_amount = 0 if current_user.account.blocked_amount.blank?
      current_user.account.blocked_amount += transaction_params[:amount].to_f.round(2)    

      # set status
      @transaction.status_id = TransactionStatus.where(name: 'Ordered').first.id;

      # save
      if current_user.account.save && @transaction.save
        # add background task for transaction
        Resque.enqueue(TransactionProcessingJob, @transaction.id)

        # redirect back to transactions
        redirect_to transactions_url(:account_id => current_user.account.id), notice: 'Transaction was successfully created.'
      else
        render :new
      end
    else
      redirect_to transactions_url(:account_id => current_user.account.id), notice: 'Sorry, you have not enough money to do the transfer.'
    end
  end

  # PATCH/PUT /transactions/1
  def update
    if @transaction.update(transaction_params)
      redirect_to @transaction, notice: 'Transaction was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /transactions/1
  def destroy
    @transaction.destroy
    redirect_to transactions_url, notice: 'Transaction was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction
      @transaction = Transaction.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transaction_params
      params.require(:transaction).permit(:account_from_id, :account_to_id, :amount, :transaction_datetime, :status_id, :user_id)
    end
end
