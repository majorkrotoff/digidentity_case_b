class TransactionStatusesController < ApplicationController
  before_action :set_transaction_status, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  
  # GET /transaction_statuses
  def index
    @transaction_statuses = TransactionStatus.all
  end

  # GET /transaction_statuses/1
  def show
  end

  # GET /transaction_statuses/new
  def new
    @transaction_status = TransactionStatus.new
  end

  # GET /transaction_statuses/1/edit
  def edit
  end

  # POST /transaction_statuses
  def create
    @transaction_status = TransactionStatus.new(transaction_status_params)

    if @transaction_status.save
      redirect_to @transaction_status, notice: 'Transaction status was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /transaction_statuses/1
  def update
    if @transaction_status.update(transaction_status_params)
      redirect_to @transaction_status, notice: 'Transaction status was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /transaction_statuses/1
  def destroy
    @transaction_status.destroy
    redirect_to transaction_statuses_url, notice: 'Transaction status was successfully destroyed.'
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_transaction_status
      @transaction_status = TransactionStatus.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def transaction_status_params
      params.require(:transaction_status).permit(:name)
    end
end
