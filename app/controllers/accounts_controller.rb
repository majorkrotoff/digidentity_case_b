class AccountsController < ApplicationController
  before_action :set_account, only: [:show, :edit, :update, :destroy, :show_transactions_list, :create_transaction]
  before_action :authenticate_user!

  # GET /accounts
  def index
    if current_user.has_role?(:admin)
      @accounts = Account.all
    else
      @accounts = Account.where(:user_id => current_user.id)
    end
  end

  # GET /accounts/1
  def show
  end

  # GET /accounts/new
  def new
    @account = Account.new
  end

  # GET /accounts/1/edit
  def edit
  end

  # POST /accounts
  def create
    @account = Account.new(account_params)
    
    if @account.save
      redirect_to @account, notice: 'Account was successfully created.'
    else
      render :new
    end
  end

  # PATCH/PUT /accounts/1
  def update
    if @account.update(account_params)
      redirect_to @account, notice: 'Account was successfully updated.'
    else
      render :edit
    end
  end

  # DELETE /accounts/1
  def destroy
    @account.destroy
    redirect_to accounts_url, notice: 'Account was successfully destroyed.'
  end


  def show_transactions_list
    redirect_to transactions_path(:account_id => @account.id)
  end

  def create_transaction
    redirect_to new_transaction_path(:account_id => @account.id)
  end    

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = Account.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def account_params
      params.require(:account).permit(:user_id, :code, :balance)
    end
end
