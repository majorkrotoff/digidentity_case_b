require 'resque/server'

Rails.application.routes.draw do
  devise_for :users

  mount Resque::Server.new, at: "/resque"
  
  root 'welcome#index'

  resources :accounts do
    member do
      get :show_transactions_list
      get :create_transaction
    end
  end

  resources :transactions

end
